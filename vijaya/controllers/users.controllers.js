const sha1 = require('sha1')
const jwt = require('jsonwebtoken')
const moment = require('moment')
var apiKey = '4a7b11fd0a75a0ef299f4bc70b19c661-de7062c6-443e3173'
var domain = 'mg.vijayacsmember.com'
var mailgun = require('mailgun-js')({apiKey: apiKey, domain: domain})
const ejs = require('ejs')
const read = require('fs').readFileSync
const join = require('path').join
const util = require('util')

module.exports = {
  register: async (req, res) => {
    let connection = req.connection
    connection.query = util.promisify(connection.query)
    let date = moment().format('YYYY-MM-DD HH:mm:ss')
    if(req.body.email_address === '' || req.body.email_address === undefined || req.body.email_address === null) {
      res.status(203).json({
        code: 203,
        msg: 'Mohon Isi Email Terlebih Dahulu',
      })
      connection.release()
      return
    }
    if(req.body.username === '' || req.body.username === undefined || req.body.username === null) {
      res.status(203).json({
        code: 203,
        msg: 'Mohon Isi Username Terlebih Dahulu',
      })
      connection.release()
      return
    }
    if(req.body.phone === '' || req.body.phone === undefined || req.body.phone === null) {
      res.status(203).json({
        code: 203,
        msg: 'Mohon Isi Phone Terlebih Dahulu',
      })
      connection.release()
      return
    }
    if(req.body.password === '' || req.body.password === undefined || req.body.password === null) {
      res.status(203).json({
        code: 203,
        msg: 'Mohon Isi Phone Terlebih Dahulu',
      })
      connection.release()
      return
    }
    if(req.body.username == req.body.email_address) {
      res.status(203).json({
        code: 203,
        msg: 'Username tidak boleh sama dengan email',
      })
      connection.release()
      return
    }
    let emails = req.body.email_address.toLowerCase()
    let queryCheck = `SELECT id FROM user WHERE email = ?`
    let queryCheckPhone = `SELECT id FROM user WHERE phone = ?`
    let queryCheckUsername = `SELECT id FROM user WHERE username = ?`
    let password = sha1(process.env.SHA1_VAR + req.body.password)
    let check = await connection.query(queryCheck, [req.body.email_address])
    if (check.length > 0) {
      
      res.status(203).json({
        code: 0,
        code_type: "exist",
        code_message: 'email already registered, please use another email address'
      })
      connection.release()
      return
    }
    let checkUsername = await connection.query(queryCheckUsername, [req.body.username])
    console.log('CHECK PHONE', checkUsername)
    if (checkUsername.length > 0) {
      
      res.status(203).json({
        code: 0,
        code_type: "exist",
        code_message: 'username already registered, please use another username'
      })
      connection.release()
      return
    }
    let checkPhone = await connection.query(queryCheckPhone, [req.body.phone])
    console.log('CHECK PHONE', checkPhone)
    if (checkPhone.length > 0) {
      
      res.status(203).json({
        code: 0,
        code_type: "exist",
        code_message: 'handphone number already registered, please use another phone'
      })
      connection.release()
      return
    }
      try {
        let insertData = {
          approved: 'non-active',
          username: req.body.username,
          phone: req.body.phone,
          password: password,
          saldo: 0,
          email: emails,
          last_paid: date,
          created: date,
          modified: date
        }
        result = await connection.query(`INSERT INTO user SET ? `, insertData)
        //
        res.status(201).json({
          code: 201,
          code_type: "Success Make List Register",
          code_message: 'Success'
        })
        connection.release()
      } catch (errCatch) {
        connection.release()
        console.log(errCatch.message)
        res.status(203).json({
          code: 203,
          code_type: "failed",
          code_message: 'Failed Register, Please Try Again.'
        })
        connection.release()
      }
  },
  listRegister: async (req,res) => {
    let connection = req.connection
    connection.query = util.promisify(connection.query)
    try {
      let listRegisterQuery = `SELECT * FROM user WHERE approved = 'non-active' ORDER BY created DESC`
      let listRegister = await connection.query(listRegisterQuery)
      res.status(200).json({
        code: 200,
        msg: 'success get list user',
        data: listRegister
      })
      connection.release()
    } catch (error) {
      res.status(400).json({
        code: 400,
        code_type: "failed get list non-active user",
        code_message: error.message
      })
      connection.release()
    }
  },
  listActiveUser: async (req,res) => {
    let connection = req.connection
    connection.query = util.promisify(connection.query)
    try {
      let listRegisterQuery = `SELECT * FROM user WHERE approved = 'active'`
      let listRegister = await connection.query(listRegisterQuery)
      res.status(200).json({
        code: 200,
        msg: 'success get list user',
        data: listRegister
      })
      connection.release()
    } catch (error) {
      res.status(400).json({
        code: 400,
        code_type: "failed get list non-active user",
        code_message: error.message
      })
      connection.release()
    }
  },
  login: async (req, res) => {
    let connection = req.connection
    connection.query = util.promisify(connection.query)
    let password = sha1(process.env.SHA1_VAR + req.body.password)
    let email = req.body.email_address
    console.log("CREDENTIALS", email, password)
    try {
      let login = await connection.query('SELECT * FROM user WHERE (username = ? OR email = ?) AND password = ? GROUP BY id', [email, email, password])
      if(login.length == 0) {
        res.status(400).json({
          code: 400,
          code_type: "username atau password anda salah"
        })
        return
        connection.release()
      } 
      console.log(login)
      let dataToken = {
        id: login[0].id,
        email_address: login[0].email,
        handphone_no: login[0].phone,
        username: login[0].username
      }
      tokens = JSON.stringify(dataToken)
      let token = jwt.sign(tokens, process.env.USER_KEY)
      res.status(200).json({
        code: 1,
        msg: 'login success!!',
        token,
        id: login[0].id,
        email_address: login[0].email,
        handphone_no: login[0].phone,
        username: login[0].username
      })
      connection.release()
      return
    } catch (error) {
      res.status(400).json({
        code: 400,
        code_type: "failed",
        code_message: error.message
      })
      connection.release()
      return
    }
  },
  forgotPassword: async (req, res) => {
    let date = moment().format('YYYY-MM-DD HH:mm:ss')
    let connection = req.connection
    connection.query = util.promisify(connection.query)
    let emails = req.body.emails
    let randomCode = Math.floor(Math.random() * 99999)
    if(emails === '' || emails === undefined || emails === null) {
      res.status(203).json({
        code: 203,
        msg: 'Mohon Isi Email Terlebih Dahulu',
      })
      connection.release()
      return
    }
    try {
      let findEmailQuery = 'SELECT * FROM user WHERE email = ?'
      let findEmail = await connection.query(findEmailQuery, emails)
      console.log('FIND EMAIL', findEmail)
      if(findEmail.length == 0) {
        res.status(203).json({
          code: 203,
          msg: 'email anda tidak ditemukan',
        })
        connection.release()
        return
      }
      let hash = sha1(process.env.SHA1_VAR + 'vijayacspassword' + randomCode)
      let newPass = {
        password: hash,
        modified: date
      }
      let changePasswordQuery = `UPDATE user SET ? WHERE email = ?`
      let changePassword = await connection.query(changePasswordQuery, [newPass, emails])
      console.log('CHANGE PASSWORD', changePassword)
      let path = join(__dirname, '../views/forgotPassword.ejs')
      let data = {
        email_address: findEmail[0].email_address,
        username: findEmail[0].username,
        password: 'vijayacspassword' + randomCode
      }
      let ret = ejs.compile(read(path, 'utf8'), { filename: path })(
        data
      )
      let dataEmail = {
        from: 'vijayacoffee <vijayacoffee@vijayacsmember.com>',
        to: emails,
        subject: 'Reset Your Password',
        html: ret
      }
      mailgun.messages().send(dataEmail)
      res.status(200).json({
        code: 200,
        msg: 'password already sent to your email',
      })
      connection.release()
      return
    } catch (error) {
      console.log('Error Forgot Password', error)
      res.status(203).json({
        code: 203,
        msg: 'server error',
      })
      connection.release()
      return
    }
  },
  updatePassword: function (req, res) {
    let connection = req.connection
    let decoded = {}
    let emails = req.body.emails
    let password = req.body.password
    if(emails === '' || emails === undefined || emails === null) {
      res.status(203).json({
        code: 203,
        msg: 'Mohon Isi Email Terlebih Dahulu',
      })
      connection.release()
      return
    }
    if(password === '' || password === undefined || password === null) {
      res.status(203).json({
        code: 203,
        msg: 'Password tidak boleh kosong',
      })
      connection.release()
      return
    }
    let date = moment().format('YYYY-MM-DD HH:mm:ss')
    connection.query('SELECT * FROM user WHERE email = ?', [emails], function (err, rows) {
      if (err) {
        connection.release()
        res.status(203).json({
          code: 0,
          msg: 'error occured when connecting to database!'
        })
        return
      }
      // update new password
      let hash = sha1(process.env.SHA1_VAR + password)
      let newPassword = hash
      let newPass = {
        password: newPassword,
        modified: date
      }
      console.log('NEW PASS', newPass)
      connection.query('UPDATE user SET ? WHERE email = ?', [newPass, emails], function (err) {
        if (!err) {
          res.status(201).json({
            code: 1,
            msg: 'update new password success!!'
          })
          connection.release()
        } else {
          // console.log('error====', err.message
          res.status(203).json({
            code: 0,
            msg: 'failed update new password'
          })
          connection.release()
        }
      })
    })
  }
}
