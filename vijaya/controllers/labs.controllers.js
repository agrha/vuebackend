const moment = require('moment')
const ejs = require('ejs')
const fs = require('fs')
const read = fs.readFileSync
const join = require('path').join
var apiKey = '4a7b11fd0a75a0ef299f4bc70b19c661-de7062c6-443e3173'
var domain = 'mg.vijayacsmember.com'
var mailgun = require('mailgun-js')({apiKey: apiKey, domain: domain})
const sms = require('../helper/sms')
const formatNum = require('format-num')
const util = require('util')
const {formatCurrency} = require('../helper/format')

module.exports = {
  getNewLabAppointment: async (req, res) => {
    let connection = req.connection
    connection.query = util.promisify(connection.query)
    try {
      let queryListLabAppointment = `SELECT l.lab_appointment_date, l.duration, l.lab_appointment_time, l.email, l.created, l.modified, l.id, l.is_deleted, l.status, u.username FROM labs l LEFT JOIN user u ON l.user_id = u.id WHERE l.status = 'new' AND l.is_deleted = 0 ORDER BY modified DESC`
      let ListLabAppointment = await connection.query(queryListLabAppointment)
      console.log(ListLabAppointment)
      res.status(200).json({
        code: 200,
        msg: 'success get lab appointment',
        data: ListLabAppointment
      })
      connection.release()
      return
    } catch (error) {
      res.status(400).json({
        code:400,
        msg: error.message
      })
      connection.release()
      return
    }
  },
  getAcceptedLabAppointment: async (req, res) => {
    let connection = req.connection
    connection.query = util.promisify(connection.query)
    try {
      let queryListLabAppointment = `SELECT l.lab_appointment_date, l.email, l.created, l.modified, l.duration, l.lab_appointment_time, l.id, l.is_deleted, l.status, u.username FROM labs l LEFT JOIN user u ON l.user_id = u.id WHERE l.status = 'accept' AND l.is_deleted = 0 ORDER BY modified DESC`
      let ListLabAppointment = await connection.query(queryListLabAppointment)
      console.log(ListLabAppointment)
      res.status(200).json({
        code: 200,
        msg: 'success get lab appointment',
        data: ListLabAppointment
      })
      connection.release()
    } catch (error) {
      res.status(400).json({
        code:400,
        msg: error.message
      })
      connection.release()
    }
  },
  getPaidLabAppointment: async (req, res) => {
    let connection = req.connection
    connection.query = util.promisify(connection.query)
    try {
      let queryListLabAppointment = `SELECT l.lab_appointment_date, l.duration, l.lab_appointment_time, l.email, l.created, l.modified, l.id, l.is_deleted, l.status, u.username FROM labs l LEFT JOIN user u ON l.user_id = u.id WHERE l.status = 'paid' AND l.is_deleted = 0 ORDER BY modified DESC`
      let ListLabAppointment = await connection.query(queryListLabAppointment)
      console.log(ListLabAppointment)
      res.status(200).json({
        code: 200,
        msg: 'success get lab appointment',
        data: ListLabAppointment
      })
      connection.release()
    } catch (error) {
      res.status(400).json({
        code:400,
        msg: error.message
      })
      connection.release()
    }
  },
  getRejectedLabAppointment: async (req, res) => {
    let connection = req.connection
    connection.query = util.promisify(connection.query)
    try {
      let queryListLabAppointment = `SELECT l.lab_appointment_date, l.email, l.duration, l.lab_appointment_time, l.created, l.modified, l.id, l.is_deleted, l.status, u.username FROM labs l LEFT JOIN user u ON l.user_id = u.id WHERE l.status = 'reject' AND l.is_deleted = 0 ORDER BY modified DESC`
      let ListLabAppointment = await connection.query(queryListLabAppointment)
      console.log(ListLabAppointment)
      res.status(200).json({
        code: 200,
        msg: 'success get lab appointment',
        data: ListLabAppointment
      })
      connection.release()
    } catch (error) {
      res.status(400).json({
        code:400,
        msg: error.message
      })
      connection.release()
    }
  },
  makeLabAppointment: async (req, res) => {
    let connection = req.connection
    connection.query = util.promisify(connection.query)
    let emails = req.body.emails
    let appointment = req.body.lab_appointment_date
    let duration = req.body.duration
    let time = req.body.lab_appointment_time
    let username = req.body.username
    if(duration === '' || duration === undefined || duration === null) {
      res.status(203).json({
        code: 203,
        msg: 'Mohon Isi Durasi Terlebih Dahulu',
      })
      connection.release()
      return
    }
    if(emails === '' || emails === undefined || emails === null) {
      res.status(203).json({
        code: 203,
        msg: 'Mohon Isi Email Terlebih Dahulu',
      })
      connection.release()
      return
    }
    if(username === '' || username === undefined || username === null) {
      res.status(203).json({
        code: 203,
        msg: 'Mohon Isi Username Terlebih Dahulu',
      })
      connection.release()
      return
    }
    if(appointment === '' || appointment === undefined || appointment === null) {
      res.status(203).json({
        code: 203,
        msg: 'Mohon Isi Tanggal Appointment Terlebih Dahulu',
      })
      connection.release()
      return
    }
    if(time === '' || time === undefined || time === null) {
      res.status(203).json({
        code: 203,
        msg: 'Mohon Isi Tanggal Appointment Terlebih Dahulu',
      })
      connection.release()
      return
    }
    let date = moment().format('YYYY-MM-DD HH:mm:ss')
    let labAppointment = {
      lab_appointment_date: appointment,
      lab_appointment_time: time,
      user_id: req.body.user_id,
      email: emails,
      duration: duration,
      notes: '',
      status:'new',
      created: date,
      modified: date
    }
    let path = join(__dirname, '../views/newUser.ejs')
    let data = {
      date: appointment,
      email: emails,
      username: req.body.username.toUpperCase()
    }
    let ret = ejs.compile(read(path, 'utf8'), {
      filename: path
    })(data)
    let dataEmail = {
      from: `vijayacoffee <vijayacoffee@vijayacsmember.com>`,
      to: 'vijayacoffee.id@gmail.com',
      subject: 'Peminjaman Lab Baru',
      html: ret
    }
    try {
      let queryMakeLab = `INSERT INTO labs SET ?`
      mailgun.messages().send(dataEmail)
      let makeEvent = await connection.query(queryMakeLab, labAppointment)
      res.status(201).json({
        code: 200,
        msg: 'success make lab appointment'
      })
      connection.release()
      } catch (error) {
        console.log(error.message)
          res.status(203).json({
          code: 203,
          msg: 'failed to make lab appointment'
        })
        connection.release()
      }
  },
  acceptLabAppointment: async (req, res) => {
    let connection = req.connection
    connection.query = util.promisify(connection.query)
    let emails = req.body.emails
    let id = req.body.id
    let appointment = req.body.lab_appointment_date
    let username = req.body.username
    let time = req.body.time
    let duration = req.body.duration
    if(emails === '' || emails === undefined || emails === null) {
      res.status(203).json({
        code: 203,
        msg: 'Mohon Isi Email Terlebih Dahulu',
      })
      connection.release()
      return
    }
    if(username === '' || username === undefined || username === null) {
      res.status(203).json({
        code: 203,
        msg: 'Mohon Isi Username Terlebih Dahulu',
      })
      connection.release()
      return
    }
    if(appointment === '' || appointment === undefined || appointment === null) {
      res.status(203).json({
        code: 203,
        msg: 'Mohon Isi Tanggal Appointment Terlebih Dahulu',
      })
      connection.release()
      return
    }
    if(time === '' || time === undefined || time === null) {
      res.status(203).json({
        code: 203,
        msg: 'Mohon Isi Waktu Appointment Terlebih Dahulu',
      })
      connection.release()
      return
    }
    if(duration === '' || duration === undefined || duration === null) {
      res.status(203).json({
        code: 203,
        msg: 'Mohon Isi Durasi Terlebih Dahulu',
      })
      connection.release()
      return
    }
    let date = moment().format('YYYY-MM-DD HH:mm:ss')
    let labAppointment = {
      status:'accept',
      modified: date
    }
    let path = join(__dirname, '../views/acceptLabAppointment.ejs')
    let data = {
      date: moment(appointment).format('LL'),
      email: emails,
      time: time,
      duration : duration,
      subtotal : formatCurrency(duration * 50000), 
      username: req.body.username.toUpperCase(),
      notes:"Transfer Ke Rekening BCA a.n SUMBER JAYA KOPI no. 8060168068 ",
    }
    let ret = ejs.compile(read(path, 'utf8'), {
      filename: path
    })(data)
    let dataEmail = {
      from: `vijayacoffee <vijayacoffee@vijayacsmember.com>`,
      to: emails,
      subject: 'Invoice Peminjaman Lab',
      html: ret
    }
    try {
      let queryUpdateLab = `UPDATE labs SET ? WHERE id = ?`
      mailgun.messages().send(dataEmail)
      let makeEvent = await connection.query(queryUpdateLab, [labAppointment, id])
      res.status(201).json({
        code: 200,
        msg: 'success make lab appointment'
      })
      connection.release()
      } catch (error) {
        console.log(error.message)
          res.status(203).json({
          code: 203,
          msg: 'failed to make lab appointment'
        })
        connection.release()
      }
  },
  paidLabAppointment: async (req, res) => {
    let connection = req.connection
    connection.query = util.promisify(connection.query)
    let paidLabAppoint
    let id = req.body.id
    let emails = req.body.emails
    let date = moment().format('YYYY-MM-DD HH:mm:ss')
    let labAppointment = {
      status:'paid',
      modified: date
    }
    let path = join(__dirname, '../views/paidMentoring.ejs')
    let data = { 
      username: req.body.username.toUpperCase(),
    }
    let ret = ejs.compile(read(path, 'utf8'), {
      filename: path
    })(data)
    let dataEmail = {
      from: `vijayacoffee <vijayacoffee@vijayacsmember.com>`,
      to: emails,
      subject: 'Pembayaran Lunas',
      html: ret
    }
    try {
      let queryUpdateLab = `UPDATE labs SET ? WHERE id = ?`
      let makeEvent = await connection.query(queryUpdateLab, [labAppointment, id])
      mailgun.messages().send(dataEmail)
      res.status(201).json({
        code: 200,
        msg: 'success change status to paid'
      })
      connection.release()
      } catch (error) {
        console.log(error.message)
          res.status(203).json({
          code: 203,
          msg: 'failed to change status'
        })
        connection.release()
      }
  },
  rejectLabAppointment: async (req, res) => {
    let connection = req.connection
    connection.query = util.promisify(connection.query)
    let id = req.body.id
    let date = moment().format('YYYY-MM-DD HH:mm:ss')
    let labAppointment = {
      status:'reject',
      modified: date
    }
    try {
      let queryUpdateLab = `UPDATE labs SET ? WHERE id = ?`
      let makeEvent = await connection.query(queryUpdateLab, [labAppointment, id])
      res.status(201).json({
        code: 200,
        msg: 'success reject lab appointment'
      })
      connection.release()
      } catch (error) {
        console.log(error.message)
          res.status(203).json({
          code: 203,
          msg: 'failed reject lab appointment'
        })
        connection.release()
      }
  },
  deleteLabAppointment: async (req, res) => {
    let connection = req.connection
    let decoded = req.decoded
    let id = req.params.id
    let payload = {
      is_deleted: 1
    }
    let queryDeleteLabAppointment = `UPDATE labs SET ? WHERE id = ?`
    try {
      let deleteEvent = await connection.query(queryDeleteLabAppointment, [payload, id])
      console.log(deleteEvent)
      res.status(200).json({
        code: 1,
        msg: 'success delete lab appointment'
      })
      connection.release()
    } catch (error) {
      console.log(error.message)
        res.status(400).json({
          code: 400,
          msg: 'failed to delete lab appointment'
        })
        connection.release()
    }
  }
}
