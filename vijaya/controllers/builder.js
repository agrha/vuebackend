// let _u = require('underscore')
// let DEFAULT_LIMIT = 10

// /**
//  * Constructor
//  * @param options Refer to README.md for a list of properties
//  * @return {Object}
//  */

// // module.exports = function (options) {
// //   let self = {
// //     sTableName: options.sTableName,
// //     sCountColumnName: options.sCountColumnName,
// //     sDatabaseOrSchema: options.sDatabaseOrSchema,
// //     aSearchColumns: options.aSearchColumns || [],
// //     sSelectSql: options.sSelectSql,
// //     sFromSql: options.sFromSql,
// //     sWhereAndSql: options.sWhereAndSql,
// //     sGroupBySql: options.sGroupBySql || [],
// //     sDateColumnName: options.sDateColumnName,
// //     dateFrom: options.dateFrom,
// //     dateTo: options.dateTo,
// //     oRequestQuery: options.oRequestQuery,
// //     sAjaxDataProp: 'data',
// //     dbType: options.dbType,

// //     buildQuery: buildQuery,
// //     parseResponse: parseResponse,
// //     extractResponseVal: extractResponseVal,
// //     filteredResult: filteredResult
// //   }
// //   console.log('self======', self)

// /**
//   * @return {string|undefined} sql statement or undefined
//   */
// // function buildSetDatabaseOrSchemaStatement () {
// //   console.log('sdatabaseorschema===', self.sDatabaseOrSchema)
// //   if (self.sDatabaseOrSchema) {
// //     if (self.dbType === 'oracle') {
// //       return 'ALTER SESSION SET CURRENT_SCHEMA = ' + self.sDatabaseOrSchema
// //     } else {
// //       return 'USE' + self.sDatabaseOrSchema
// //     }
// //   }
// // }

// /**
// * @return {*}
// */

// // function buildDatePartial () {
// //   if (self.sDateColumnName && slef.dateFrom || self.dateTo) {
// //     console.log("date from to===", self.dateFrom, self.dateTo)
// //     if(self.dateFrom && self.dateTo) {
// //       return self.sDateColumnName + "BETWEEN '" + self.dateFrom.toISOString() + "' AND '"+ self.dateTo.toISOString() + "'"
// //     } else if(self.dateFrom){
// //       return self.sDateColumnName + " >= '" + self.dateFrom.toISOString() + "'"
// //     } else if (self.dateTo) {
// //       return self.sDateColumnName + " >= '" + self.dateTo.toISOString() + "'"
// //     }
// //   }
// //   return undefined
// // }

// /**
//  * (private) Build a complete SELECT statement that counts the number of entries.
//  * @param searchString If specified then produces a statement to count the filtered list of records.
//  * Otherwise the statement counts the unfiltered list of records.
//  * @return {String} A complete SELECT statement
//  */

// function buildCountStatement(requestQuery) {
//   let dateSql = buildDatePartial()
//   let result;
//   if(self.sGroupBySql.length) {
//     result = "SELECT COUNT (*) FROM ("
//     result += "SELECT COUNT(" + self.sGroupBySql[0] + ") FROM "
//     result += self.sFromSql ? self.sFromSql : self.sTableName
//     result += buildWherePartial(requestQuery)
//     result += buildGroupByPartial()
//     result += ") temp"
//   } else {
//     console.log('masuk else buildcountstat==')
//     result = "SELECT COUNT ("
//     result += self.sSelectSql ? "*" : (self.sCountColumnName ? self.sCountColumnName : "id")
//     result += ") FROM "
//     result += self.sFromSql ? self.sFromSql : self.sTableName
//     result += buildWherePartial(requestQuery)
//   }
//   console.log('result buildcountstat====', result)
//   return result
// }

// /**
//  *
//  * @param searchString
//  * @return {String}
//  */

// function buildWherePartial(requestQuery) {
//   console.log('buildwherepartial====', requestQuery)
//   let sWheres = []
//   let searchQuery = buildSearchPartial(requestQuery)
//   if(searchQuery)
//     sWheres.push(searchQuery)
//   if(self.sWhereAndSql)
//     sWheres.push(self.sWhereAndSql)
//   let dateSql = buildDatePartial()
//   if(dateSql)
//     sWheres.push(dateSql)
//   if(sWheres.length)
//     return " WHERE (" + sWheres.join(") AND (") + ")"
//   return ""
// }

// /**
//  * (private) Build the GROUP BY clause
//  * @return {String}
//  */

// function buildGroupByPartial() {
//   if(self.sGroupBySql.length)
//     return " GROUP BY " + self.sGroupBySql.join(',') + " "
//   return ""
// }

// /**
//  * (private)  Builds the search portion of the WHERE clause using LIKE (or ILIKE for PostgreSQL).
//  * @param {Object} requestQuery
//  * @return {String}
//  */

// function buildSearchPartial(requestQuery) {
//   let searches = []
//   let colSearches = buildSearchArray(requestQuery)
//   let globalSearches = buildSearchArray(requestQuery, true);

//   if(colSearches.length){
//     searches.push('(' + colSearches.join(" AND ") + ')');
//   }
//   if(globalSearches.length){
//     searches.push('(' + globalSearches.join(" OR ") + ')');
//   }
//   return searches.join(" AND ");
// }

// /**
//  * (private) Builds an array of LIKE / ILIKE statements to be added to the WHERE clause
//  * @param {Object} requestQuery
//  * @param {*} [global]
//  * @returns {Array}
//  */
// function buildSearchArray(requestQuery, global) {
//   let searchArray = []
//   let customColumns = _u.isArray(self.aSearchColumns) && !_u.isEmpty(self.aSearchColumns) && global
//   _u.each(customColumns ? self.aSearchColumns : requestQuery.columns, function(column){
//     if(customColumns || column.searchable === 'true'){
//       let colName = sanitize(customColumns ? column : column.name)
//       let searchVal = sanitize(global ? requestQuery.search.value : column.search.value)
//       if(colName && searchVal){
//         searchArray.push(self.dbType === 'postgres' ?
//           buildILIKESearch(colName, searchVal) :
//           buildLIKESearch(colName, searchVal))
//       }
//     }
//   })
//   return searchArray;
// }

// /**
//  * (private) Builds the search portion of the WHERE clause using ILIKE
//  * @param {string} colName
//  * @param {string} searchVal
//  * @returns {string}
//  */
// function buildILIKESearch(colName, searchVal) {
//   return "CAST(" + colName + " as text)" + " ILIKE '%" + searchVal + "%'"
// }

// /**
//  * (private) Builds the search portion of the WHERE clause using LIKE
//    * @param {string} colName
//    * @param {string} searchVal
//    * @returns {string}
//    */
// function buildLIKESearch(colName, searchVal) {
//   return colName + " LIKE '%" + searchVal + "%'"
// }

// /**
//  * (private) Adds an ORDER clause
//  * @param requestQuery
//  * @return {String}
// */
// function buildOrderingPartial(requestQuery) {
//   let query = []
//   let l = _u.isArray(requestQuery.order) ? requestQuery.order.length : 0
//   for(let fdx = 0; fdx < l; ++fdx) {
//     let order = requestQuery.order[fdx]
//     let column = requestQuery.columns[order.column]
//     if(column.orderable === 'true' && column.name) {
//       query.push(column.name + " " + order.dir)
//     }
//   }
//   if(query.length)
//     return " ORDER BY " + query.join(", ");
//   return "";
// }

// /**
//  * Build a LIMIT clause
//  * @param requestQuery
//  * @return {String}
//  */

// function buildLimitPartial(requestQuery) {
//   let sLimit = ""
//   if(requestQuery && requestQuery.start !== undefined && self.dbType !== 'oracle') {
//     let start = parseInt(requestQuery.start, 10)
//     if(start >= 0) {
//       let len = parseInt(requestQuery.length, 10)
//       sLimit = (self.dbType === 'postgres') ? " OFFSET " + String(start) + " LIMIT " : " LIMIT " + String(start) + ", "
//       sLimit += ( len > 0 ) ? String(len) : String(DEFAULT_LIMIT)
//     }
//   }
//   return sLimit
// }

// /**
//  * Build the base SELECT statement.
//  * @return {String}
//  */
// function buildSelectPartial() {
//   let query = "SELECT "
//   query += self.sSelectSql ? self.sSelectSql : "*"
//   query += " FROM "
//   query += self.sFromSql ? self.sFromSql : self.sTableName
//   return query
// }

// /**
//  * Build an array of query strings based on the Datatable parameters
//  * @param requestQuery
//  * @return {Object}
// */

// function buildQuery(requestQuery) {
//   console.log('buildquery====', requestQuery)
//   let queries = {}
//   if(typeof requestQuery !== 'object')
//     return queries
//   let searchString = sanitize(_u.isObject(requestQuery.search) ? requestQuery.search.value : '')
//   self.oRequestQuery = requestQuery
//   let useStmt = buildSetDatabaseOrSchemaStatement()
//   if(useStmt) {
//     queries.changeDatabaseOrSchema = useStmt
//   }
//   queries.recordsTotal = buildCountStatement(requestQuery)
//   if(searchString) {
//     queries.recordsFiltered = buildCountStatement(requestQuery)
//   }
//   let query = buildSelectPartial()
//   query += buildWherePartial(requestQuery)
//   query += buildGroupByPartial()
//   query += buildOrderingPartial(requestQuery)
//   query += buildLimitPartial(requestQuery)
//   if(self.dbType === 'oracle'){
//     let start = parseInt(requestQuery.start, 10)
//     let len = parseInt(requestQuery.length, 10)
//     if(len >= 0 && start >= 0) {
//       query = 'SELECT * FROM (SELECT a.*, ROWNUM rnum FROM (' + query + ') '
//       query += 'a)' + ' WHERE rnum BETWEEN ' + (start + 1) + ' AND ' + (start + len)
//     }
//   }
//   queries.select = query;
//   return queries;
// }

// /**
//  * Parse the responses from the database and build a Datatable response object.
//  * @param queryResult
//  * @return {Object}
//  */
// // function parseResponse(queryResult) {
// //   let oQuery = self.oRequestQuery
// //   let result = { recordsFiltered: 0, recordsTotal: 0 }
// //   if(oQuery && typeof oQuery.draw === 'string') {
// //     result.draw = parseInt(oQuery.draw,10);
// //   } else{
// //     result.draw = 0
// //   }
// //   if(_u.isObject(queryResult) && _u.keys(queryResult).length > 1) {
// //     result.recordsFiltered = result.recordsTotal = extractResponseVal(queryResult.recordsTotal) || 0;
// //     if(queryResult.recordsFiltered) {
// //       result.recordsFiltered = extractResponseVal(queryResult.recordsFiltered) || 0;
// //     }
// //     result.data = queryResult.select
// //   }
// //   return result
// // }

// /**
//  * (private) extract value from a database response
//  * @param {Array}
//  * @return {*}
//  */

// //  function extractResponseVal(res) {
// //    console.log("extravtresponse res ====", res)
// //    if(_u.isArray(res) && res.length && _u.isObject(res[0])) {
// //      let resObj = _u.values(res[0])
// //      if(resObj.length){
// //        return resObj[0]
// //      }
// //    }
// //  }

// /**
// * debug, reduced size objecr for display
//   * @param obj
//   * @return {*}
//   */
// //   function filteredResult(obj, count) {
// //     if(obj) {
// //       let result = _u.omit(obj, self.sAjaxDataProp)
// //       result.aaLength = obj[self.sAjaxDataProp] ? obj[self.sAjaxDataProp].length : 0
// //       result[self.sAjaxDataProp] = []
// //       let count = count ? Math.min(count, result.aaLength) : result.aaLength
// //       for(let idx = 0; idx < count; ++idx) {
// //         result[self.sAjaxDataProp].push(obj[self.sAjaxDataProp][idx])
// //       }
// //       console.log('result filtered===', result)
// //       return result
// //     }
// //     return null
// //   }
// //   return self

// // }

// /**
//  * sanitize to prevent SQL injection
//  * @param str
//  * @return {*}
//  */

// // function sanitize(str, len) {
// //   console.log('sanitize=== str, len', str, len)
// //   if(!str || typeof str === 'string' && str.length < 1)
// //     return str
// //   if(typeof str !== 'string' && str.length > len)
// //      return null
// //   return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
// //     switch (char) {
// //       case "\0":
// //         return "\\0";
// //       case "\x08":
// //         return "\\b";
// //       case "\x09":S
// //         return "\\t";
// //       case "\x1a":
// //         return "\\z";
// //       case "\n":
// //         return "\\n";
// //       case "\r":
// //         return "\\r";
// //       case "\"":
// //       case "'":
// //       case "\\":
// //       case "%":
// //         return "\\" + char
// //     }
// //   })
// // }
