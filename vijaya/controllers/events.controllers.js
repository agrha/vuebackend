// const jwt = require('jsonwebtoken')
// const supplierKey = process.env.SUPPLIER_KEY
const moment = require('moment')
const util = require('util')
const ejs = require('ejs')
const fs = require('fs')
const read = fs.readFileSync
var apiKey = '4a7b11fd0a75a0ef299f4bc70b19c661-de7062c6-443e3173'
var domain = 'mg.vijayacsmember.com'
var mailgun = require('mailgun-js')({apiKey: apiKey, domain: domain})
const cloudinary = require('cloudinary')
const Datauri = require('datauri')
const {formatCurrency} = require('../helper/format')
const join = require('path').join

cloudinary.config({
  cloud_name: process.env.CLOUDINARY_NAME,
  api_key: process.env.CLOUDINARY_KEY,
  api_secret: process.env.CLOUDINARY_SECRET
})

module.exports = {
  getListEvent: async (req, res) => {
    let connection = req.connection
    connection.query = util.promisify(connection.query)
    try {
      let queryListEvent = `SELECT * FROM event WHERE is_deleted = 0`
      let listEvent = await connection.query(queryListEvent)
      res.status(200).json({
        code: 200,
        msg: 'success get event',
        data: listEvent
      })
      connection.release()
    } catch (error) {
      res.status(400).json({
        code:400,
        msg: error.message
      })
      connection.release()
    }
  },
  makeEvent: async (req, res) => {
    let connection = req.connection
    connection.query = util.promisify(connection.query)
    let image
    if(req.files && req.files.length > 0) {
      let formatFile = '.' + files.image.originalname.split('.').pop()
      let fileName = md5(Date.now() + files.image.originalname)
      datauri.format(formatFile, files.image.buffer)
      let uploadFile = await cloudinary.v2.uploader.upload(datauri.content, {public_id: 'product/' + fileName})
      let variant_image = fileName + '.' + uploadFile.format
      image = fileName + '.' + uploadFile.format
    } else {
      image = req.body.image
    }
    let date = moment().format('YYYY-MM-DD HH:mm:ss')
    let event = {
      m_name: req.body.m_name,
      m_date: req.body.m_date,
      m_image: image,
      m_name: req.body.m_name,
      m_place: req.body.m_place,
      m_topic: req.body.m_topic,
      created: date,
      modified: date
    }
    let queryMakeEvent = `INSERT INTO event SET ?`
    try {
      let makeEvent = await connection.query(queryMakeEvent, event)
      res.status(201).json({
        code: 201,
        msg: 'success make event',
      })
      connection.release()
    } catch (error) {
      console.log(error.message)
        res.status(400).json({
          code:0,
          msg: 'failed to make event'
        })
        connection.release()
    }
  },
  updateEvent: async (req, res) => {
    let connection = req.connection
    connection.query = util.promisify(connection.query)
    let image = req.body.image
    let date = moment().format('YYYY-MM-DD HH:mm:ss')
    let event = {
      m_date: req.body.m_date,
      m_image: image,
      m_name: req.body.m_name,
      m_place: req.body.m_place,
      m_topic: req.body.m_topic,
      created: date,
      modified: date
    }
    let queryUpdateEvent = `UPDATE event SET ? WHERE id = ?`
    try {
      let makeEvent = await connection.query(queryUpdateEvent, [event, req.body.id])
      res.status(201).json({
        code: 201,
        msg: 'success update event',
      })
      connection.release()
    } catch (error) {
      console.log(error.message)
        res.status(400).json({
          code:0,
          msg: 'failed to update event'
        })
        connection.release()
    }
  },
  registerEvent: async (req, res) => {
    let connection = req.connection
    connection.query = util.promisify(connection.query)
    let user_id = req.body.user_id
    let event_id = req.body.event_id
    let appointment_date = req.body.appointment_date
    let appointment_time = req.body.appointment_time
    let duration = req.body.duration
    if(appointment_date === '' || appointment_date === undefined || appointment_date === null) {
      res.status(203).json({
        code: 203,
        msg: 'Appointment date Mohon diisi',
      })
      connection.release()
      return
    }
    if(appointment_time === '' || appointment_time === undefined || appointment_time === null) {
      res.status(203).json({
        code: 203,
        msg: 'Appointment time Mohon diisi',
      })
      connection.release()
      return
    }
    if(duration === '' || duration === undefined || duration === null) {
      res.status(203).json({
        code: 203,
        msg: 'Durasi Mohon diisi',
      })
      connection.release()
      return
    }
    if(user_id === '' || user_id === undefined || user_id === null) {
      res.status(203).json({
        code: 203,
        msg: 'User Id Kosong',
      })
      connection.release()
      return
    }
    if(event_id === '' || event_id === undefined || event_id === null) {
      res.status(203).json({
        code: 203,
        msg: 'Event Id Kosong',
      })
      connection.release()
      return
    }
    let found = `SELECT COUNT(id) as total, status FROM userevent WHERE userid = ${req.body.user_id} AND eventid = ${req.body.event_id}`
    let checkUserQuery = `SELECT COUNT(id) as total, email, username FROM user WHERE id = ${req.body.user_id}`
    let checkEventQuery = `SELECT COUNT(id) as total FROM event WHERE id = ${req.body.event_id}`
    let date = moment().format('YYYY-MM-DD HH:mm:ss')
    let event = {
      appointment_date:appointment_date,
      appointment_time:appointment_time,
      duration: duration,
      eventid: event_id,
      userid: user_id,
      status:'new',
      created: date,
      modified: date
    }
    
    let queryMakeEvent = `INSERT INTO userevent SET ?`
    try {
      let checkUser = await connection.query(checkUserQuery)
      let checkEvent = await connection.query(checkEventQuery)
      console.log('CHECK USER', checkUser)
      if(checkUser[0].total == 0) {
        res.status(203).json({
          code: 203,
          msg: 'User Tidak Ditemukan',
        })
        connection.release()
        return
      }
      if(checkEvent[0].total == 0) {
        res.status(203).json({
          code: 203,
          msg: 'Event Tidak Ditemukan',
        })
        connection.release()
        return
      }
      let count = await connection.query(found)
      console.log('COUNT', count)
      // if(count[0].total > 0) {
      //   if(count[0].status = 'new') {
      //     res.status(203).json({
      //       code: 203,
      //       msg: 'Permohonan Anda Sedang Diproses, Mohon Menunggu Konfirmasi',
      //     })
      //     connection.release()
      //     return
      //   } else if(count[0].status = 'accept') {
      //     res.status(203).json({
      //       code: 203,
      //       msg: 'Anda Sudah Mendaftar Pada Event ini',
      //     })
      //     connection.release()
      //     return
      //   }
      // }
      let path = join(__dirname, '../views/newMentoring.ejs')
      let data = {
        date: appointment_date,
        email: checkUser[0].email,
        username: checkUser[0].username.toUpperCase()
      }
      console.log('DATA', data)
      let ret = ejs.compile(read(path, 'utf8'), {
        filename: path
      })(data)
      let dataEmail = {
        from: `vijayacoffee <vijayacoffee@vijayacsmember.com>`,
        to: 'vijayacoffee.id@gmail.com',
        subject: 'Permintaan Mentoring Baru',
        html: ret
      }
      let makeEvent = await connection.query(queryMakeEvent, event)
      res.status(201).json({
        code: 201,
        msg: 'Anda Berhasil Mendaftar, mohon menunggu konfirmasi dari tim vijayacoffee',
      })
      connection.release()
      return
    } catch (error) {
      console.log(error.message)
      res.status(400).json({
        code:0,
        msg: 'gagal mendaftar, mohon coba lagi dalam beberapa menit'
      })
      connection.release()
      return
    }
  },
  acceptRegisterEvent: async (req,res) => {
    let connection = req.connection
    connection.query = util.promisify(connection.query)
    let date = moment().format('YYYY-MM-DD HH:mm:ss')
    usereventid = req.body.usereventid
    let checkDataQuery = `SELECT u.username, u.email, ue.appointment_date, ue.appointment_time, ue.duration, ue.eventid FROM userevent ue LEFT JOIN user u ON ue.userid = u.id WHERE ue.id = ${usereventid}`
    let user = await connection.query(checkDataQuery)
    console.log("USER USER EVENT", user[0])
    let queryUpdate = `UPDATE userevent SET ? WHERE id = ?`
    let payload = {
      status: 'accept',
      modified: date,
    }
    let path = join(__dirname, '../views/acceptMentoring.ejs')
    let data = {
      durasi: user[0].duration,
      subtotal: formatCurrency(user[0].duration * 50000),
      is_dropshipper: 0,
      mentor: (user[0].eventid == 1) ? "Eric Lim": "William Sicher Wijaya",
      notes:"Transfer Ke Rekening BCA a.n SUMBER JAYA KOPI no. 8060168068 ",
      date: moment(user[0].appointment_date).format('LL')
    }
    let ret = ejs.compile(read(path, 'utf8'), {
      filename: path
    })(data)
    let dataEmail = {
      from: `vijayacoffee <vijayacoffee@vijayacsmember.com>`,
      to: user[0].email,
      subject: 'Invoice Mentoring',
      html: ret
    }
    try {
      let result = await connection.query(queryUpdate, [payload, usereventid])
      mailgun.messages().send(dataEmail)
      res.status(200).json({
        code: 200,
        msg: 'success update mentoring'
      })
      connection.release()
      return
    } catch (error) {
      console.log(error.message)
      res.status(400).json({
        code: 400,
        msg: 'failed to update mentoring'
      })
      connection.release()
      return
    }
  },
  paidRegisterEvent: async (req,res) => {
    let connection = req.connection
    connection.query = util.promisify(connection.query)
    let date = moment().format('YYYY-MM-DD HH:mm:ss')
    usereventid = req.body.usereventid
    let emailQuery = `SELECT u.email from userevent ue LEFT JOIN user u on ue.userid = u.id WHERE ue.id = ${usereventid}`
    let email = await connection.query(emailQuery)
    let queryUpdate = `UPDATE userevent SET ? WHERE id = ?`
    let payload = {
      status: 'paid',
      modified: date
    }
    let data = {}
    let path = join(__dirname, '../views/paidMentoring.ejs')
    let ret = ejs.compile(read(path, 'utf8'), {
      filename: path
    })(data)
    let dataEmail = {
      from: `vijayacoffee <vijayacoffee@vijayacsmember.com>`,
      to: email[0].email,
      subject: 'Pembayaran Lunas',
      html: ret
    }
    try {
      let result = await connection.query(queryUpdate, [payload, usereventid])
      mailgun.messages().send(dataEmail)
      res.status(200).json({
        code: 200,
        msg: 'success delete event'
      })
      connection.release()
      return
    } catch (error) {
      console.log(error.message)
      res.status(400).json({
        code: 400,
        msg: 'failed to update userevent'
      })
      connection.release()
      return
    }
  },
  rejectRegisterEvent: async (req,res) => {
    let connection = req.connection
    connection.query = util.promisify(connection.query)
    usereventid = req.body.usereventid
    let date = moment().format('YYYY-MM-DD HH:mm:ss')
    let queryUpdate = `UPDATE userevent SET ? WHERE id = ?`
    let payload = {
      status: 'reject',
      modified: date
    }
    try {
      let result = await connection.query(queryUpdate, [payload, usereventid])
      res.status(200).json({
        code: 200,
        msg: 'success delete event'
      })
      connection.release()
      return
    } catch (error) {
      console.log(error.message)
      res.status(400).json({
        code: 400,
        msg: 'failed to update userevent'
      })
      connection.release()
      return
    }
  },
  getNewListUserEvent: async (req, res) => {
    let connection = req.connection
    connection.query = util.promisify(connection.query)
    try {
      let queryListUserEvent = `SELECT ue.id as id, ue.status as status, u.id AS user_id, ue.appointment_date, ue.appointment_time, ue.duration, u.username AS username, u.phone AS phone, u.email AS email, e.id as event_id FROM userevent ue LEFT JOIN user u ON ue.userid = u.id LEFT JOIN event e ON ue.eventid = e.id WHERE ue.status = 'new' AND ue.is_deleted = 0 AND ue.eventid = 1`
      let queryListUserEvent2 = `SELECT ue.id as id, ue.status as status, u.id AS user_id, ue.appointment_date, ue.appointment_time, ue.duration, u.username AS username, u.phone AS phone, u.email AS email, e.id as event_id FROM userevent ue LEFT JOIN user u ON ue.userid = u.id LEFT JOIN event e ON ue.eventid = e.id WHERE ue.status = 'new' AND ue.is_deleted = 0 AND ue.eventid = 2`
      let event1 = await connection.query(queryListUserEvent)
      let event2 = await connection.query(queryListUserEvent2)
      res.status(200).json({
        code: 200,
        msg: 'success get lab appointment',
        data: {
          event1: event1,
          event2: event2
        }
      })
      connection.release()
      return
    } catch (error) {
      res.status(400).json({
        code:400,
        msg: error.message
      })
      connection.release()
      return
    }
  },
  getAcceptListUserEvent: async (req, res) => {
    let connection = req.connection
    connection.query = util.promisify(connection.query)
    try {
      let queryListUserEvent = `SELECT ue.id as id, ue.appointment_date, ue.appointment_time, ue.duration, ue.status as status, u.id AS user_id, u.username AS username, u.phone AS phone, u.email AS email, e.id as event_id FROM userevent ue LEFT JOIN user u ON ue.userid = u.id LEFT JOIN event e ON ue.eventid = e.id WHERE ue.status = 'accept' AND ue.is_deleted = 0 AND ue.eventid = 1`
      let queryListUserEvent2 = `SELECT ue.id as id, ue.appointment_date, ue.appointment_time, ue.duration,  ue.status as status, u.id AS user_id, u.username AS username, u.phone AS phone, u.email AS email, e.id as event_id FROM userevent ue LEFT JOIN user u ON ue.userid = u.id LEFT JOIN event e ON ue.eventid = e.id WHERE ue.status = 'accept' AND ue.is_deleted = 0 AND ue.eventid = 2`
      let event1 = await connection.query(queryListUserEvent)
      let event2 = await connection.query(queryListUserEvent2)
      res.status(200).json({
        code: 200,
        msg: 'success get lab appointment',
        data: {
          event1: event1,
          event2: event2
        }
      })
      connection.release()
      return
    } catch (error) {
      res.status(400).json({
        code:400,
        msg: error.message
      })
      connection.release()
      return
    }
  },
  getRejectListUserEvent: async (req, res) => {
    let connection = req.connection
    connection.query = util.promisify(connection.query)
    try {
      let queryListUserEvent = `SELECT ue.id as id,  ue.appointment_date, ue.appointment_time, ue.duration, ue.status as status, u.id AS user_id, u.username AS username, u.phone AS phone, u.email AS email, e.id as event_id FROM userevent ue LEFT JOIN user u ON ue.userid = u.id LEFT JOIN event e ON ue.eventid = e.id WHERE ue.status = 'reject' AND ue.is_deleted = 0 AND ue.eventid = 1`
      let queryListUserEvent2 = `SELECT ue.id as id, ue.appointment_date, ue.appointment_time, ue.duration, ue.status as status, u.id AS user_id, u.username AS username, u.phone AS phone, u.email AS email, e.id as event_id FROM userevent ue LEFT JOIN user u ON ue.userid = u.id LEFT JOIN event e ON ue.eventid = e.id WHERE ue.status = 'reject' AND ue.is_deleted = 0 AND ue.eventid = 2`
      let event1 = await connection.query(queryListUserEvent)
      let event2 = await connection.query(queryListUserEvent2)
      res.status(200).json({
        code: 200,
        msg: 'success get lab appointment',
        data: {
          event1: event1,
          event2: event2
        }
      })
      connection.release()
      return
    } catch (error) {
      res.status(400).json({
        code:400,
        msg: error.message
      })
      connection.release()
      return
    }
  },
  getPaidListUserEvent: async (req, res) => {
    let connection = req.connection
    connection.query = util.promisify(connection.query)
    try {
      let queryListUserEvent = `SELECT ue.id as id, ue.appointment_date, ue.appointment_time, ue.duration, ue.status as status, u.id AS user_id, u.username AS username, u.phone AS phone, u.email AS email, e.id as event_id FROM userevent ue LEFT JOIN user u ON ue.userid = u.id LEFT JOIN event e ON ue.eventid = e.id WHERE ue.status = 'paid' AND ue.is_deleted = 0 AND ue.eventid = 1`
      let queryListUserEvent2 = `SELECT ue.id as id, ue.appointment_date, ue.appointment_time, ue.duration, ue.status as status, u.id AS user_id, u.username AS username, u.phone AS phone, u.email AS email, e.id as event_id FROM userevent ue LEFT JOIN user u ON ue.userid = u.id LEFT JOIN event e ON ue.eventid = e.id WHERE ue.status = 'paid' AND ue.is_deleted = 0 AND ue.eventid = 2`
      let event1 = await connection.query(queryListUserEvent)
      let event2 = await connection.query(queryListUserEvent2)
      res.status(200).json({
        code: 200,
        msg: 'success get lab appointment',
        data: {
          event1: event1,
          event2: event2
        }
      })
      connection.release()
      return
    } catch (error) {
      res.status(400).json({
        code:400,
        msg: error.message
      })
      connection.release()
      return
    }
  },
  deleteEvent: async (req, res) => {
    let date = moment().format('YYYY-MM-DD HH:mm:ss')
    let connection = req.connection
    connection.query = util.promisify(connection.query)
    let decoded = req.decoded
    let id = req.params.id
    let payload = {
      is_deleted: 1,
      modified: date
    }
    let queryDeleteEvent = `UPDATE event SET ? WHERE id = ?`
    try {
      let deleteEvent = await connection.query(queryDeleteEvent, [payload, id])
      console.log(deleteEvent)
      res.status(200).json({
        code: 1,
        msg: 'success delete event'
      })
      connection.release()
      return
    } catch (error) {
      console.log(error.message)
      res.status(400).json({
        code:0,
        msg: 'failed to delete event'
      })
      connection.release()
      return
    }
  },
  deleteUserEvent: async (req, res) => {
    let date = moment().format('YYYY-MM-DD HH:mm:ss')
    let connection = req.connection
    connection.query = util.promisify(connection.query)
    let decoded = req.decoded
    let id = req.params.id
    let payload = {
      is_deleted: 1,
      modified: date
    }
    let queryDeleteEvent = `UPDATE userevent SET ? WHERE id = ?`
    try {
      let deleteEvent = await connection.query(queryDeleteEvent, [payload, id])
      console.log(deleteEvent)
      res.status(200).json({
        code: 1,
        msg: 'success delete event'
      })
      connection.release()
      return
    } catch (error) {
      console.log(error.message)
      res.status(400).json({
        code:0,
        msg: 'failed to delete event'
      })
      connection.release()
      return
    }
  }
}
