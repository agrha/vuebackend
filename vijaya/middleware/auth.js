const jwt = require('jsonwebtoken')
const userKey = process.env.USER_KEY

module.exports = {
  jwtAuth: (req, res, next) => {
    let connection = req.connection
    jwt.verify(req.headers.token, userKey, (err, decoded) => {
      if (err) {
        res.status(400).json({
          msg: err.message
        })
        connection.release()
      } else {
        if(decoded){
          req.decoded = decoded
        }
        if(req.decoded.id === undefined){
          res.status(400).json({
            msg: "Sorry, you cant access this account for a moment. please contact support@vijayamail.com"
          })
          connection.release()
        }

        let checkQuery = `SELECT * FROM user WHERE email = '${req.decoded.email_address}'`
          connection.query(checkQuery, (errCheck, result) => {
            if (!errCheck && result.length > 0) {
              req.decoded.id = result[0].id
              next()
            }else{
              res.status(400).json({
                code:0,
                msg: "Unauthentication"
              })
              connection.release()
            }
          })
      }
    })
  }
}
