var createError = require('http-errors')
var express = require('express')
var path = require('path')
var cors = require('cors')
var cookieParser = require('cookie-parser')
var logger = require('morgan')
const mysql = require('mysql')
const session = require('express-session')
require('dotenv').config()

var indexRouter = require('./routes/index')
let usersRouter = require('./routes/users')
const eventsRouter = require('./routes/events')
const labsRouter = require('./routes/labs')

var app = express()
app.use(express.static(__dirname, { dotfiles: 'allow' } ))
app.use(cors())

const pool = mysql.createPool({
  connectionLimit: 10,
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME,
  debug: false
})

app.use(function (req, res, next) {
  pool.getConnection((err, connection) => {
    if (err) {
      console.log(err)
      let message = ''
      if (err.code === 'PROTOCOL_CONNECTION_LOST') {
        message = 'Database connection was closed.'
        console.error(message)
      }
      if (err.code === 'ER_CON_COUNT_ERROR') {
        message = 'Database has too many connections.'
        console.error(message)
      }
      if (err.code === 'ECONNREFUSED') {
        message = 'Database connection was refused.'
        console.error(message)
      }
      res.status(400).json({
        msg: message
      })
    } else {
      req.connection = connection
      next()
    }
  })
})

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(session({ secret: 'kittens', resave: false, saveUninitialized: true }))


app.use('/', indexRouter)
app.use('/api/v1/users', usersRouter)
app.use('/api/v1/events', eventsRouter)
app.use('/api/v1/labs', labsRouter)


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404))
})

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app
