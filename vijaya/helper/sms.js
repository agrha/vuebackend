var axios = require('axios')

function sms(phone, content, cb) {
  let phoneNumber = phone.substring(0, 2)
  switch (phoneNumber) {
    case '08':
      phone = '628' + phone.substring(2)
      break
    case '62':
      phone = phone
      break
    default:
      phone = undefined
      break
  }
  if (phone !== undefined) {
    axios
      .get(process.env.SMS_GATEWAY_URL, {
        params: {
          usr: process.env.SMS_GATEWAY_USR,
          pwd: process.env.SMS_GATEWAY_PWD,
          msg: content,
          msisdn: phone
        }
      })
      .then(function(response) {
        //console.log(response)
        cb()
      })
      .catch(function(error) {
        console.log(error)
      })
  }
}

module.exports = sms
