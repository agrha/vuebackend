var OneSignal = require('onesignal-node')

function onesignalPush (playerId = [], content) {
  let onesignal = new OneSignal.Client({
    userAuthKey: process.env.ONESIGNAL_USERAUTHKEY,
    app: { appAuthKey: process.env.ONESIGNAL_APPAUTHKEY, appId: process.env.ONESIGNAL_APPID }
  })
  let firstNotification = new OneSignal.Notification({
    contents: {
      en: content
    },
    include_player_ids: playerId
  })
  onesignal.sendNotification(firstNotification, function (err, httpResponse, data) {
    if (err) {
      console.log('Onesignal - Something went wrong...')
    } else {
      console.log('Onesignal - Success')
    }
  })
}

module.exports = {onesignalPush}
