
const numberWithDots = (x) => {
    if (x == null) { return ''}
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

const formatCurrency = (x) => {
    return 'Rp. ' + numberWithDots(x)
}

const formatCurrencyNonRp = (x) => {
    return numberWithDots(x)
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

module.exports = {numberWithDots, formatCurrency, formatCurrencyNonRp, validateEmail}
