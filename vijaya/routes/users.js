var express = require('express')
var router = express.Router()
const { jwtAuth } = require('../middleware/auth')

const {
  register,
  listRegister,
  verifyRegister,
  login,
  forgotPassword,
  updatePassword,
  listActiveUser
} = require('../controllers/users.controllers')

router.post('/register', register)
router.get('/list-register', listRegister)
router.get('/list-active-user', listActiveUser)
router.post('/login', login)
router.post('/forgot-password', forgotPassword)
router.post('/update-password', updatePassword)

module.exports = router
