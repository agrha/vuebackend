const express = require('express')
const router = express.Router()
const {
    getListEvent,
    makeEvent,
    deleteEvent,
    registerEvent,
    acceptRegisterEvent,
    rejectRegisterEvent,
    getNewListUserEvent,
    getAcceptListUserEvent,
    getRejectListUserEvent,
    getPaidListUserEvent,
    updateEvent,
    paidRegisterEvent

} = require('../controllers/events.controllers')

const { jwtAuth } = require('../middleware/auth')

router.get('/get-list-event', getListEvent)
router.get('/get-new-list-user-event', getNewListUserEvent), 
router.get('/get-accept-list-user-event', getAcceptListUserEvent), 
router.get('/get-reject-list-user-event', getRejectListUserEvent), 
router.get('/get-paid-list-user-event', getPaidListUserEvent), 
router.post('/register-event', registerEvent)
router.post('/accept-register-event', acceptRegisterEvent)
router.post('/reject-register-event', rejectRegisterEvent)
router.post('/paid-register-event', paidRegisterEvent)
router.post('/make-event', makeEvent)
router.post('/update-event', updateEvent)
router.post('/delete-event/:id', deleteEvent)

module.exports = router