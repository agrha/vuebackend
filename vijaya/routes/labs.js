
var express = require('express')
const { jwtAuth } = require('../middleware/auth')
var router = express.Router()
const {
  getNewLabAppointment,
  getAcceptedLabAppointment,
  getRejectedLabAppointment,
  makeLabAppointment,
  acceptLabAppointment,
  rejectLabAppointment,
  deleteLabAppointment,
  paidLabAppointment,
  getPaidLabAppointment
} = require('../controllers/labs.controllers')
const multer = require('multer')
let upload = multer({
  storage: multer.memoryStorage(),
  limits: {
    fileSize: 1 * 1024 * 1024
  }
})

router.get('/get-lab-appointment', getNewLabAppointment)
router.get('/get-accept-lab-appointment', getAcceptedLabAppointment)
router.get('/get-rejected-lab-appointment', getRejectedLabAppointment)
router.get('/get-paid-lab-appointment', getPaidLabAppointment)
router.post('/make-lab-appointment', makeLabAppointment)
router.post('/accept-lab-appointment', acceptLabAppointment)
router.post('/reject-lab-appointment', rejectLabAppointment)
router.post('/paid-lab-appointment', paidLabAppointment)
router.post('/delete-lab-appointment/:id', deleteLabAppointment)


module.exports = router
